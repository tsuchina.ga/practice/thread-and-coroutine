import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) = runBlocking {

    val time = measureTimeMillis {
        val jobs = List(10) { _ ->

            launch {
                println("[%s]%dms start".format(Thread.currentThread(), System.currentTimeMillis()))

                var i: Long = 0
                (0..3_000_000_000).forEach { i += it }

                println("[%s]%dms end".format(Thread.currentThread(), System.currentTimeMillis()))
            }

        }

        jobs.forEach { it.join() }
    }

    println(time)
}
